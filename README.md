# makla-hw-v0

This is an obsolete early version of the *makla* hardware.
It combines keyboard inputs, processing, audio generation and audio output in one PCB.
It uses dedicated signal generating ICs for synthesizing the audio output frequencies.
This suffers from only very few waveforms being available and low-power (high-impedance) audio outputs.
